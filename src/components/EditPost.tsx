import React, { useEffect, useState } from "react";
import { getPost } from "../graphql/queries";
import { UpdatePostInput, GetPostQuery } from "../API";
import { updatePost } from "../graphql/mutations";
import { API, graphqlOperation, Auth } from "aws-amplify";
import { GraphQLResult } from "@aws-amplify/api";

type Props = {
  id: string;
  onCloseClicked: React.MouseEventHandler<HTMLElement>;
};

const EditPost = ({ id, onCloseClicked }: Props) => {
  const [postOwnerId, setPostOwnerId] = useState("");
  const [postOwnerUsername, setPostOwnerUsername] = useState("");
  const [postTitle, setPostTitle] = useState<string | undefined>();
  const [postBody, setPostBody] = useState<string | undefined>();

  useEffect(() => {
    const getUserInfo = async () => {
      const userInfo = await Auth.currentUserInfo();
      setPostOwnerId(userInfo.attributes.sub);
      setPostOwnerUsername(userInfo.username);
    };
    getUserInfo();
  }, []);

  useEffect(() => {
    const retrievePost = async () => {
      if (id == null || id === "") return;
      const post = (await API.graphql(
        graphqlOperation(getPost, { id })
      )) as GraphQLResult<GetPostQuery>;

      setPostTitle(post.data?.getPost?.postTitle);
      setPostBody(post.data?.getPost?.postBody);
    };
    retrievePost();
  }, [id]);

  const onTitleChange = (event: React.FormEvent<HTMLInputElement>) => {
    setPostTitle(event.currentTarget.value);
  };

  const onBodyChange = (event: React.ChangeEvent<HTMLTextAreaElement>) => {
    setPostBody(event.currentTarget.value);
  };

  const onEditPost = async (
    event: React.MouseEvent<HTMLButtonElement, MouseEvent>
  ) => {
    event.preventDefault();

    const input: UpdatePostInput = {
      id: id,
      postOwnerId: postOwnerId!,
      postOwnerUsername: postOwnerUsername!,
      postTitle: postTitle!,
      postBody: postBody!,
      createdAt: new Date().toISOString(),
    };

    await API.graphql(graphqlOperation(updatePost, { input }));
    setPostTitle("");
    setPostBody("");
    onCloseClicked(event);
  };

  return (
    <>
      <i className="close icon" onClick={onCloseClicked}></i>
      <form className="ui form">
        <div className="field">
          <label>Title</label>
          <input
            type="text"
            name="title"
            placeholder="Title"
            value={postTitle}
            onChange={onTitleChange}
          ></input>
        </div>
        <div className="field">
          <label>Body</label>
          <textarea
            name="body"
            placeholder="Body"
            value={postBody}
            onChange={onBodyChange}
          ></textarea>
        </div>
        <button className="ui button" type="submit" onClick={onEditPost}>
          Submit
        </button>
      </form>
    </>
  );
};

export default EditPost;
