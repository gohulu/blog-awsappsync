import React, { useEffect, useState } from "react";
import { listPosts } from "../graphql/queries";
import { API, graphqlOperation } from "aws-amplify";
import { GraphQLResult } from "@aws-amplify/api";
import { ListPostsQuery, Post } from "../API";
import {
  onCreatePost,
  onDeletePost,
  onUpdatePost,
} from "../graphql/subscriptions";
import { deletePost } from "../graphql/mutations";
import { Observable } from "zen-observable-ts";

import EditPost from "./EditPost";

const DisplayPosts = () => {
  const [posts, setPosts] = useState<Post[] | null>(null);
  const [selectedId, setSelectedId] = useState("");
  const [updatePostShown, setUpdatePostShown] = useState(false);

  useEffect(() => {
    const getPosts = async () => {
      const result = (await API.graphql(
        graphqlOperation(listPosts)
      )) as GraphQLResult<ListPostsQuery>;
      setPosts(result.data?.listPosts?.items as Post[]);
    };

    getPosts();
  }, []);

  useEffect(() => {
    const observable = API.graphql(
      graphqlOperation(onCreatePost)
    ) as Observable<any>;
    const createPostListener = observable.subscribe({
      next: (postData: { value: { data: { onCreatePost: any } } }) => {
        const newPost = postData.value.data.onCreatePost;
        const prevPosts = posts?.filter((post) => post.id !== newPost.id)!;
        setPosts([newPost, ...prevPosts]);
      },
    });

    return () => createPostListener.unsubscribe();
  }, [posts]);

  useEffect(() => {
    const observable = API.graphql(
      graphqlOperation(onUpdatePost)
    ) as Observable<any>;
    const createDeleteListener = observable.subscribe({
      next: (postData: { value: { data: { onUpdatePost: any } } }) => {
        const updatedPost = postData.value.data.onUpdatePost;
        const postWithoutUpdated = posts?.filter(
          (post) => post.id !== updatedPost.id
        )!;
        setPosts([updatedPost, ...postWithoutUpdated]);
      },
    });

    return () => createDeleteListener.unsubscribe();
  }, [posts]);

  useEffect(() => {
    const observable = API.graphql(
      graphqlOperation(onDeletePost)
    ) as Observable<any>;
    const createPostListener = observable.subscribe({
      next: (postData: { value: { data: { onDeletePost: any } } }) => {
        const newPost = postData.value.data.onDeletePost;
        const updatedPosts = posts?.filter((post) => post.id !== newPost.id)!;
        setPosts(updatedPosts);
      },
    });

    return () => createPostListener.unsubscribe();
  }, [posts]);

  const onDeleteButtonClicked = async (id: string) => {
    const input = {
      id,
    };
    await API.graphql(graphqlOperation(deletePost, { input }));
  };

  const onEditButtonClicked = async (id: string) => {
    setSelectedId(id);
    setUpdatePostShown(true);
  };

  return (
    <>
      {updatePostShown && (
        <EditPost
          onCloseClicked={() => setUpdatePostShown(false)}
          id={selectedId}
        />
      )}
      <h2 className="ui header">Posts</h2>
      <div className="ui comments">
        {posts &&
          posts.map((post) => (
            <div key={post.id} className="comment">
              <div className="content">
                <div className="author">{post.postOwnerUsername}</div>
                <div className="metadata">
                  <span className="date">
                    {new Date(post.createdAt as string).toDateString()}
                  </span>
                </div>
                <div className="text">{post.postBody}</div>
                <div className="actions">
                  <button
                    className="ui button"
                    onClick={() => onEditButtonClicked(post.id)}
                  >
                    Edit
                  </button>
                  <button
                    className="ui button"
                    onClick={() => onDeleteButtonClicked(post.id)}
                  >
                    Delete
                  </button>
                </div>
              </div>
            </div>
          ))}
      </div>
    </>
  );
};

export default DisplayPosts;
