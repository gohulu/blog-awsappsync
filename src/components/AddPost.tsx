import React, { useEffect, useState } from "react";
import { CreatePostInput } from "../API";
import { createPost } from "../graphql/mutations";
import { API, graphqlOperation, Auth } from "aws-amplify";

const AddPost: React.FC = () => {
  const [postOwnerId, setPostOwnerId] = useState<string>("");
  const [postOwnerUsername, setPostOwnerUsername] = useState<string>("");
  const [title, setTitle] = useState<string>("");
  const [body, setBody] = useState<string>("");

  useEffect(() => {
    const getCurrentUser = async () => {
      const user = await Auth.currentUserInfo();
      setPostOwnerUsername(user.username);
      setPostOwnerId(user.attributes.sub);
    };

    getCurrentUser();
  }, []);
  const onAddPost = async (
    event: React.MouseEvent<HTMLButtonElement, MouseEvent>
  ) => {
    event.preventDefault();

    const input: CreatePostInput = {
      postOwnerId: postOwnerId!,
      postOwnerUsername: postOwnerUsername!,
      postTitle: title!,
      postBody: body!,
      createdAt: new Date().toISOString(),
    };

    await API.graphql(graphqlOperation(createPost, { input }));
    setTitle("");
    setBody("");
  };

  const onTitleChange = (event: React.FormEvent<HTMLInputElement>) => {
    setTitle(event.currentTarget.value);
  };

  const onBodyChange = (event: React.ChangeEvent<HTMLTextAreaElement>) => {
    setBody(event.currentTarget.value);
  };

  return (
    <form className="ui form">
      <div className="field">
        <label>Title</label>
        <input
          type="text"
          name="title"
          placeholder="Title"
          value={title}
          onChange={onTitleChange}
        ></input>
      </div>
      <div className="field">
        <label>Body</label>
        <textarea
          name="body"
          placeholder="Body"
          value={body}
          onChange={onBodyChange}
        ></textarea>
      </div>
      <button className="ui button" type="submit" onClick={onAddPost}>
        Submit
      </button>
    </form>
  );
};

export default AddPost;
