import React from "react";
import AddPost from "./components/AddPost";
import DisplayPosts from "./components/DisplayPosts";
import { withAuthenticator } from "aws-amplify-react";

const App: React.FC = () => (
  <div className="ui raised very padded text container segment">
    <AddPost />
    <DisplayPosts />
  </div>
);

export default withAuthenticator(App, true);
